<?php

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function revision_author_tokens_token_info() {
  $info = [];

  $info['tokens']['node']['revision-author'] = [
    'name' => t("Revision Author"),
    'description' => t("The name of the user who created the current revision of the node."),
  ];

  $info['tokens']['node']['revision-author-uid'] = [
    'name' => t("Revision Author UID"),
    'description' => t("The UID of the user who created the current revision of the node."),
  ];

  $info['tokens']['node']['revision-author-mail'] = [
    'name' => t("Revision Author Email"),
    'description' => t("The email of the user who created the current revision of the node."),
  ];

  return $info;
}

/**
 * Implements hook_tokens().
 */
function revision_author_tokens_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
    $replacements = [];
  
    if ($type == 'node' && !empty($data['node'])) {
      foreach ($tokens as $name => $original) {
        switch ($name) {
          case 'revision-author':
            $revision_author = $data['node']->getRevisionUser();
            $replacements[$original] = $revision_author ? $revision_author->getDisplayName() : '';
            break;
          
          case 'revision-author-uid':
            $revision_author = $data['node']->getRevisionUser();
            $replacements[$original] = $revision_author ? $revision_author->id() : '';
            break;
      
          case 'revision-author-mail':
            $revision_author = $data['node']->getRevisionUser();
            $replacements[$original] = $revision_author ? $revision_author->getEmail() : '';
            break;
        }
      }
    }
  
    return $replacements;
  }