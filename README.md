# Node Revision Author Tokens

The Revision Author Tokens module extends Drupal's token system to include tokens related to the author of node revisions. It allows site builders and developers to use these tokens in various places like views, rules, and custom blocks to display information about the revision author.

## Features

This module adds the following tokens for nodes:

- `[node:revision-author]`: The name of the user who created the current revision of the node.
- `[node:revision-author-uid]`: The UID of the user who created the current revision of the node.
- `[node:revision-author-email]`: The email address of the user who created the current revision of the node.

## Requirements

- Drupal 10+
- Token module

## Installation

1. Download and install the Token module if you haven't already.
2. Place the Revision Author Tokens module in your Drupal installation under the `modules/custom` directory or `sites/default/modules/custom`.
3. Enable the Revision Author Tokens module via the Drupal admin interface (Administer > Extend) or by using Drush with the command `drush en revision_author_tokens`.